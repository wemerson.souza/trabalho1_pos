const http = require('http');
const url = require('url');
const {createHash} = require('crypto');
const port = 3000;

const resultados = {
	pessoas: [{id:1, nome: "Marcelo"}, {id:2, nome: "Joao"}, {id:3, nome: "Maria"}],
	carros: [{id:1, modelo: "Fusca"}, {id:2, modelo: "Gol"}, {id:3, modelo: "Palio"}],
	animais: [{id:1, nome: "Cachorro"}, {id:2, nome: "Gato"}, {id:3, nome: "Papagaio"}]
	}
	
const fs = require("fs");

const calculateHash = (str) => {
	const hash = createHash('sha256');
	hash.update(str);
	return hash.digest('hex');
}

const server = http.createServer((req, res) => {
	const parsedUrl = url.parse(req.url, true);
	const path = parsedUrl.path;
	var reqNoneMatch = req.headers['if-none-match'];
	console.info(path);
	if (path === "/pessoas") {
		var chtml = createpage(1)
		if (reqNoneMatch == calculateHash(chtml)) {
			res.writeHead(304);
			res.end();
			return;
		}
		res.writeHead(200,
			{
				'cache-control': "max-age=10, must-revalidate",
				"etag": calculateHash(chtml)
			});
		res.end(chtml);
	} else if (path === "/carros") {
		var chtml = createpage(2)
		if (reqNoneMatch == calculateHash(chtml)) {
			res.writeHead(304);
			res.end();
			return;
		}
		res.writeHead(200,
			{
				'cache-control': "max-age=10, must-revalidate",
				"etag": calculateHash(chtml)
			});
		res.end(chtml);
	}else if (path === "/animais") {
		var chtml = createpage(3)
			if (reqNoneMatch == calculateHash(chtml)) {
				res.writeHead(304);
				res.end();
				return;
			}
			res.writeHead(200,
				{
					'cache-control': "max-age=10, must-revalidate",
					"etag": calculateHash(chtml)
				});
			res.end(chtml);
	}else if(path === "/"){
		var chtml = createpage(0)
			if (reqNoneMatch == calculateHash(chtml)) {
				res.writeHead(304);
				res.end();
				return;
			}
			res.writeHead(200,
				{
					'cache-control': "max-age=10, must-revalidate",
					"etag": calculateHash(chtml)
				});
			res.end(chtml);
	}else{
		res.writeHead(404);
		res.end('<html lang="en"><body><h1>Page Doesn\'t exist<h1></body></html>');
	}
});

const createpage = (page) => {

	var html = ''

	if(page == 1){
		var pessoas = resultados.pessoas;//[{id:1, nome: "Marcelo"}, {id:2, nome: "João"}, {id:3, nome: "Maria"}, {id:5, nome: "Mariana"}];
		var listaPessoas = pessoas.map(function(item, indice){
			return `<tr>
						<td>${item.id	}</td>
						<td>${item.nome}</td>
					</tr>`;
		 });

		 html = `<html>
		<head>
			<title>PESSOAS</title>
		</head>
		<body>
			<table id="tbPessoas">
					<thead>
						<tr>
							<th>id</th>
							<th>Nome</th>
						</tr>
						${listaPessoas.join("")}
					</thead>
					<tbody>
				</tbody>
			</table>	
		</body>
		</html>`;
	}else if(page ==2){
		var carros = resultados.carros;//[{id:1, modelo: "Fusca"}, {id:2, modelo: "Gol"}, {id:3, modelo: "Palio"}];
		var listaCarros = carros.map(function(item, indice){
			return `<tr>
						<td>${item.id	}</td>
						<td>${item.modelo}</td>
					</tr>`;
		 });

		 html = `<html>
		<head>
			<title>Carros</title>
		</head>
		<body>
			<table id="tbCarros">
					<thead>
						<tr>
							<th>id</th>
							<th>Nome</th>
						</tr>
						${listaCarros.join("")}
					</thead>
					<tbody>
				</tbody>
			</table>	
		</body>
		</html>`;
	}else if(page == 3 ){
		var animais = resultados.animais;//[{id:1, nome: "Cachorro"}, {id:2, nome: "Gato"}, {id:3, nome: "Papagaio"}, {id:4, nome: "Peixe"}];
		var listaAnimais = animais.map(function(item, indice){
			return `<tr>
						<td>${item.id	}</td>
						<td>${item.nome}</td>
					</tr>`;
		 });
		 html = `<html>
		<head>
			<title>Animais</title>
		</head>
		<body>
			<table id="tbAnimais">
					<thead>
						<tr>
							<th>id</th>
							<th>Nome</th>
						</tr>
						${listaAnimais.join("")}
					</thead>
					<tbody>
				</tbody>
			</table>	
		</body>
		</html>`;

	}else{
		html = `<html>
				<head>
					<title>Pagina inicial</title>
				</head>
				<body>
					<h1>Pagina Inicial</h1>
					<a href="/pessoas">Pessoas</a>
					<a href="/carros">Carros</a>
					<a href="/animais">Animais</a>
				</body>
				</html>`;
	}

	return html
}

server.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
